# frozen_string_literal: true

require 'date'

class Team
  attr_accessor :name, :template, :title

  def initialize(name, template, title)
    @name = name
    @template = template
    @title = title
  end
end
