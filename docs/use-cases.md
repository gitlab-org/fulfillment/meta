## Use Cases

### Subscription Activation

| Has activation code | Subscription type | Feature flags | Subscription activation display |
| ------ | ------ | ------ | ------ |
| ✅ | - | - | online
| ✅ | - | `automated_email_provision` | legacy |
| ✅ | - | `automated_email_provision`, `strict_cloud_license` | legacy |
| ✅ | - | `strict_cloud_license` | legacy |
| ❌ | - | - | legacy
| ✅ | `online_cloud_license` | `automated_email_provision` | online |
| ✅ | `online_cloud_license` | `automated_email_provision`, `strict_cloud_license` | online |
| ✅ | `online_cloud_license` | `strict_cloud_license` | online |
| ❌ | `online_cloud_license` | `automated_email_provision` | legacy |
| ❌ | `online_cloud_license` | `automated_email_provision`, `strict_cloud_license` | legacy |
| ❌ | `online_cloud_license` | `strict_cloud_license` | legacy |
| ✅ | `offline_cloud_license` | `automated_email_provision` | offline |
| ✅ | `offline_cloud_license` | `automated_email_provision`, `strict_cloud_license` | offline |
| ✅ | `offline_cloud_license` | `strict_cloud_license` | offline |
| ❌ | `offline_cloud_license` | `automated_email_provision` | legacy |
| ❌ | `offline_cloud_license` | `automated_email_provision`, `strict_cloud_license` | legacy |
| ❌ | `offline_cloud_license` | `strict_cloud_license` | legacy |
| ✅ | `cloud_license_disabled` | `automated_email_provision` | legacy |
| ✅ | `cloud_license_disabled` | `automated_email_provision`, `strict_cloud_license` | new legacy |
| ✅ | `cloud_license_disabled` | `strict_cloud_license` | new legacy |
| ❌ | `cloud_license_disabled` | `automated_email_provision` | legacy |
| ❌ | `cloud_license_disabled` | `automated_email_provision`, `strict_cloud_license` | legacy |
| ❌ | `cloud_license_disabled` | `strict_cloud_license` | legacy |
| ✅ | `cloud_license_undecided` | `automated_email_provision` | online |
| ✅ | `cloud_license_undecided` | `automated_email_provision`, `strict_cloud_license` | online |
| ✅ | `cloud_license_undecided` | `strict_cloud_license` | online |
| ❌ | `cloud_license_undecided` | `automated_email_provision` | legacy |
| ❌ | `cloud_license_undecided` | `automated_email_provision`, `strict_cloud_license` | legacy |
| ❌ | `cloud_license_undecided` | `strict_cloud_license` | legacy |
