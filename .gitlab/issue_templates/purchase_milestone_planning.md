<!--

Note: there is a helpful checklist at the end of the description for a how-to fill this Issue

-->

[[_TOC_]]

# Self-Service and Purchase group responsibilities

- Zuora HPM integration
- Gitlab.com SaaS new purchase flows (SaaS Plan, CI Minutes, Storage)
- Customer Portal Self-Managed new purchase flow
- Community Programs new signup flow
- Community Programs renewal flow
  - will move to ~"group::billing and subscription management" once launched, keeping in Purchase b/c of engineers that started development

# `Milestone XX.YY` Milestone Information

* Start date: `XX.YY milestone start date`
* End date: `XX.YY milestone end date`
* [**Issue board**]()
  * Issues within each list are prioritized with the highest priority issues ranked from top to bottom, please pick issues in order wherever possible
  * Issues marked with ~Deliverable are issues we are committing to deliver in the current milestone


##   Milestone Summary

### :lifter: &nbsp; Planned capacity

We estimate engineering working capacity for the purpose of planning a realistic milestone (by combining planned time off and a number of working days in the milestone) and **not for measuring individual performance**.

<details><summary>Click to see the breakdown of team capacity for the milestone</summary>

&nbsp;<br/>
**Formula: 1 engineer = 10 weight / milestone (`XX work days` in XX.YY milestone) = 0.5 weight per day**


| Team | Engineer | # Working Days => # Weight (rounded up) |
| ------ | ------ | ------ |
| FS | Mohamed | `XX` (\*`.5`) => XX |
| FE | Angelo | `XX` (\*`.5`) => XX |
| FE | Vamsi | `XX` (\*`.5`) => XX |
| ~frontend | **TOTAL** | ** XX ** |
| BE | Josianne | `XX` (\*`.5`) => XX |
| BE | Bishwa | `XX` (\*`.5`) => XX |
| ~backend | **TOTAL** | ** XX ** |

</details>


### :bug: &nbsp; Priority Bugs


| Priority | Issue | Status | Teams |
|-------|-------|:------|:------:|
|  |  |  |  |


### :star: &nbsp; Priority Projects

Please refer to the [Fulfillment Roadmap](https://docs.google.com/spreadsheets/d/17IfBrltEWM49z6__NxbyuYkqdIWRAdqIJ6_UHgOa0no/edit#gid=1276538540&fvid=733428927) for an up-to-date list of all of our priorities.


| Epic / Issue | Status | Teams | Target Milestone |
|-------|:------|:------:|:------:|
|  |  |  |  |


### :hammer_pick: &nbsp; Technical debt

This list contains technical debt Issues that we wish to tackle in the milestone.

| Issue | Weight |
|-------|------|
|  |

### :frame_photo: &nbsp; In Design

Take a look at what our designers are working on [here]().


# Team Holiday Time

Please add your holiday dates below for the `XX.YY milestone` release period and don't forget to add to the Fulfillment calendar.

| :calendar_spiral: | [Family & Friends :family:](https://about.gitlab.com/company/family-and-friends-day/#upcoming-family-and-friends-days) | Public Holiday :ferris_wheel: | Vacation :island: | Total days off |
| :---|:---:|:---:|:---:|:---:|
| **Product Manager**| | | | |
| Priyanka  |  |  |  |  |
| **Product Designer** | | | | |
| Tim    |  |  |  |  |
| **Engineering Manager** | | | | |
| ~~Shreays~~ (Parental leave)   |  |  |  |  |
| Sharmad (Acting)   |  |  |  |  |
| **Software Engineers: Fullstack** | | | | |
| Mohamed |  |  |  |  |
| **Software Engineers: Frontend** | | | | |
| Angelo   |  |  |  |  |
| Vamsi    |  |  |  |  |
| **Software Engineers: Backend** | | | | |
| Josianne |  |  |  |  |
| Bishwa   |  |  |  |  |

# Planning Issue checklist

<details><summary>2 weeks before: Planning Issue</summary>

- [ ] Create `next XX.YY milestone` Purchase Planning issue
  - [ ] Create the issue (prepend `[Draft]` to the title)
  - [ ] Review Priority Projects, ensure it’s clear what we will be focusing on for the milestone
</details>

<details><summary>2 weeks before: Milestone Board</summary>

- [ ] Create `next XX.YY milestone` Purchase Board
  - [ ] Create the board
  - [ ] Add columns (use the previous milestone board as an example for which columns to display)
  - [ ] Review issues (e.g. move out anything that we won’t get to and keep in mind issues from the previous milestones that will roll over)
  - [ ] Make sure the candidate issues have the ~"workflow::refinement"

</details>

<details><summary>1 week before: Milestone Board consolidation and review</summary>

- [ ] Finalize [Next Milestone Board](https://gitlab.com/groups/gitlab-org/-/boards/5078830)
  - [ ] All issues (that are labelled ~backend and ~frontend) should have weight
  - [ ] Review all of the issues again, now that the previous milestone issues have been rolled over. Re-order to reflect prioriies
  - [ ] Post a comment on the Issue and ask the team to review the Board

</details>

<details>
<summary>1 week to 1 day before: moving Issues</summary>

- [ ] Move issues from [Active Milestone Board](https://gitlab.com/groups/gitlab-org/-/boards/5019876) to [Next Milestone Board](https://gitlab.com/groups/gitlab-org/-/boards/5078830):
  - [ ] First review all of the Open issues to see what can be closed
  - [ ] Capture weights of what’s been done VS still open. This data will be used when we’re wrapping up the milestone (e.g., [this comment](https://gitlab.com/gitlab-org/fulfillment-meta/-/issues/970#note_1284579922)), and will also go into this spreadsheet
  - [ ] ~frontend
    - [ ] Total closed - X weight; Y issues
    - [ ] Unplanned - X weight; Y issues
    - [ ] Pushed to next/later milestone - X weight; Y issues
  - [ ] ~backend
    - [ ] Total closed - X weight; Y issues
    - [ ] Unplanned - X weight; Y issues
    - [ ] Pushed to next/later milestone - X weight; Y issues
  - [ ] Update the milestone values for [Active Milestone Board](https://gitlab.com/groups/gitlab-org/-/boards/5019876) and [Next Milestone Board](https://gitlab.com/groups/gitlab-org/-/boards/5078830).

</details>

<details><summary>Start of the milestone: closing previous Issues</summary>

- [ ] `Date milestone starts` - Close `XX.YY-1` Purchase Planning issue
  - [ ] Post a comment similar to this one, or whatever else you’d like to highlight
  - [ ] Close issue

</details>

<details><summary>Start of the milestone: Planning Issue finalisation </summary>

- [ ] Finalize `next XX.YY milestone` Purchase Planning issue
  - [ ] Re-review Priority Projects, ensure it’s clear what we will be focusing on for the milestone
  - [ ] Capture weights of all issues, so we know the total before the milestone start
  - [ ] Update this spreadsheet
  - [ ] Remove `[Draft]` from the title
  - [ ] Post to the team slack channel that both the Board and Issue are ready 🥳

</details>
/confidential
