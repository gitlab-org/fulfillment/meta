[[_TOC_]]

## Step 1: Describe what this request is about by answering the following questions

### What is the problem/opportunity?  

<!-- 
    What is the problem you or the customer has encountered? If you were to frame is as an Opportunity, what might that be? 
-->

### Who is it for?

<!-- 
    Who is experiencing the problem?  Who gains to benefit from the solution?  Are there any stakeholders that are impacted?
    Example
       1. Internal: Finance, Billing, Support, IT, EntAps, etc
       2. External: paid SaaS or SM customers, publicSector, trial users, free users/community program users, etc

-->

### Why is this important?

<!--
 Include details to help better understand the problem/opportunity, and to help us prioritize 
    1. customer data, subscription data  
    2. dashboard
-->

### How do you propose we solve for it?

<!-- 
    What ideas do you have for solutions? 
-->


### What does success look like, and how can we measure that?

<!--
    This will help us understand whether we have solved the problem / addressed the opportunity. 
-->



## Step 2: Assign to Fulfillment Leads

<!--
 At the moment, this is @courtmeddaugh and @jeromeng
-->

## Step 3: Fulfillment begins review of intake request.

<!-- 
    If this request has a component of Zuora and/or SFDC, please tag @gitlab-com/business-technology/enterprise-apps/zuora-architects for visibility 
-->



## Step 4: Fulfillment approves, defers, or cancels the request


/label ~"workflow::validation backlog" 
/label ~"product work"
