### Welcome to the Fulfillment team! :tada: :tada: :tada:

This issue is meant to guide new UX members as they get familiar with the role. Please use this issue to ask any questions or reach out to @jackib or another team member if you need any help during your onboarding (and afterwards).

### New team member

## General Tasks
- [ ] Slack - Make sure you join:
    - [#s_fulfillment](https://gitlab.slack.com/archives/CMJ8JR0RH) - General Fulfillment section channel
    - [#s_fulfillment_fyi](https://gitlab.slack.com/archives/C042N0EET9N) - Fulfillment announcements
    - [#s_fulfillment_daily](https://app.slack.com/client/T02592416/C01BNLX4085) - Async Daily Standups (uses Geekbot)
    - [Relevant keeping yourself informed channels](https://about.gitlab.com/handbook/communication/chat/)
- [ ] :wave: Your [onboarding buddy](#onboarding-buddy) will be _Onboarding buddy_. Your onboarding buddy is here to help with any onboarding tasks.

## Background
- [ ] :books: Check out the [direction](https://about.gitlab.com/direction/fulfillment/) of Fulfillment
- [ ] :tools: Read about how we work in [Fulfillment](https://about.gitlab.com/handbook/engineering/development/fulfillment/#how-we-work)
- [ ] Review and bookmark [workflow documentation](https://www.figma.com/file/DCq7K8Srsv79tbH1yRkGbl/Document-user-flows-%5Bgitlab-org%2F-%2Fepics%2F3603%5D?node-id=1%3A2) that the UX team creates and maintains

## Setting up your environment
- [ ] :computer: Download and install [CustomersDot](https://gitlab.com/gitlab-org/customers-gitlab-com/-/blob/staging/doc/installation_steps.md). The installation processes can be a little complicated, please feel free to reach out to someone in the `#s_fulfillment` Slack channel for help at any time.
- [ ] Ask for a Zuora admin in the `#s_fulfillment` Slack channel to set up your Zuora sandbox account.

## Social
- [ ] :coffee: Have some coffee chats with [other team members](https://about.gitlab.com/handbook/product/product-categories/#fulfillment-section) in engineering, UX and PM.


## Learn the product
- [ ] Create a new issue in this project and name it something like "UX Walkthrough of Fulfillment Flows". Complete the following user tasks in GitLab SaaS as if you are a user and jot down your thoughts and ideas about how you think we can improve. Remember this is the only time you'll do these tasks with the eyes of a new user! Your onboarding buddy can answer questions about testing in production.
    - [ ] Create gitlab.com user by signing up for a new account, create a group/project and start a trial
    - [ ] Create another group and make a purchase
    - [ ] Paid group, add users, upgrade bronze to silver
    - [ ] Set your paid account to auto-renew
    - [ ] Manually renew and change the number of users
- [ ] Review your findings with your PM and other designers on the team. Create new issues for anything you discovered that is not already known. This task will take some time, but at the end of it you will be familiar with the area of the product we work on, as well as the roadmap, and known and unknown UX problems to solve.
- [ ] Pick one of the tasks to do in your local environment so you can get familiar with how this works.

## Start your first issues
- [ ] Ready to work on something? During your first weeks, your PM will select a few issues for you to work on that aren't too complex so that you can learn the user flows and how everything works together. Make sure you assign yourself to the issue, apply the [right workflow labels](https://about.gitlab.com/handbook/engineering/development/fulfillment/#organizing-the-work) and the current milestone. Ping your onboarding buddy or your manager for any help.
- [ ] Please review this onboarding issue and update the template with some improvements as you see fit to make it easier for the next newbie!

## Get a bit of technical background (these items are optional for UX, depending on your level of interest/comfort in front end development)...
- [ ] :tv: Have a look at [Behind the scenes of a purchase flow in the Customer Portal](https://www.youtube.com/watch?v=Y1EC7mCLqiE)
- [ ] :tv: Check out [a video on how various Fulfillment systems work together from a Backend perspective](https://youtu.be/iPP0vplNSmY).
- [ ] :books: Read about [the portal integrations](https://about.gitlab.com/handbook/business-ops/enterprise-applications/portal/), [subscriptions](https://about.gitlab.com/handbook/marketing/product-marketing/enablement/dotcom-subscriptions/), and [licensing](https://about.gitlab.com/pricing/licensing-faq/) FAQ.
- [ ] :books: Read the [development documentation for the customers app](https://gitlab.com/gitlab-org/customers-gitlab-com/-/tree/staging/doc)

## Manager
1. [ ] Add team member [to the Fulfillment calendar](https://calendar.google.com/calendar/embed?src=gitlab.com_7199q584haas4tgeuk9qnd48nc%40group.calendar.google.com&ctz=Europe%2FMadrid)
1. [ ] Assist team member with getting added to the Fulfillment Social Calendar.
1. [ ] Add team member to the retrospective (update [membership](https://gitlab.com/gl-retrospectives/fulfillment) and [config file](https://gitlab.com/gitlab-org/async-retrospectives/-/blob/master/teams.yml)) -
1. [ ] Invite team member to #fulfillment-social Slack channel
1. [ ] Update team page and product category page with the right team and manager
1. [ ] Request a manager update in BambooHR
1. [ ] Add team member to `@fulfillment-group` on GitLab.com


