# {name} Frontend to Fullstack Engineer Training

## Plan

- [ ] [Learn Ruby the Hard Way book](https://learnrubythehardway.org/book/)
- [ ] [The Odin Project - Ruby](https://www.theodinproject.com/paths/full-stack-ruby-on-rails/courses/ruby)
- [ ] [Ruby on Rails Tutorial - Michael Hartl](https://www.railstutorial.org/book)

## Projects

- [ ] [The Odin Project](https://www.theodinproject.com/paths/full-stack-ruby-on-rails/courses/ruby-on-rails)
- [ ] Habit trackers
- [ ] Weather app
- [ ] Note taking app
- [ ] Recipe app
- [ ] Reddit clone
- [ ] Pinterest
- [ ] Todoist clone

## Resources

- [Thoughbot Upcase](https://thoughtbot.com/upcase/practice) pick episodes to fill the gaps
- [GoRails](https://gorails.com/episodes) pick episodes to fill the gaps
- [Web-Crunch](https://www.youtube.com/channel/UCzePijHDYnVHIXW6HYTL4dg) pick episodes to fill the gaps
- [Ruby Style Guide](https://github.com/rubocop/ruby-style-guide)

### Extra Resources

- [Ruby Toolbox - Find actively maintained & popular open source libraries for Ruby](https://www.ruby-toolbox.com/)
- [Awesome Ruby](https://github.com/markets/awesome-ruby)
- [Building A Full-Stack Application With Vue, Vuex and Rails](https://www.honeybadger.io/blog/building-app-rails-vue-vuex/)
- [Introduction To RSpec](https://www.theodinproject.com/lessons/ruby-introduction-to-rspec)

## Rails topics to cover

- [ ] Application structure
- [ ] Request / Response cycle
  - Request
  - Routes
  - Controllers
  - Views (layout, helpers)
  - Models
- [ ] Scaffolding
- [ ] Migrations
- [ ] The schema file
- [ ] Seeds
- [ ] Indexes
- [ ] CRUD
- [ ] Model
  - Validation
  - Associations (belongs_to, has_one, has_many)
  - Queries (find, where, scope, join, includes, grouping)
  - Lifecycle methods
- [ ] Views
  - Templates
  - Layouts
  - Partials
  - Helpers
  - Forms (nested forms)
- [ ] Controllers
  - Actions
  - Params (strong params, sessions, flash)
  - Request / Response
  - Render / Redirect
  - Filters (Auth tokens)
- [ ] Debugging
  - Debugger
  - Pry-rails
  - Logging
- [ ] Testing
- [ ] Exceptions
- [ ] Concerns
- [ ] Assets pipeline
  - Sprockets
  - Import maps
  - Concatenation & minification
  - Fingerprinting
  - Manifest files
  - Source maps
  - Assets compilation
- [ ] Rails as API provider
  - Simple API provider
  - Rails API + VueJS
- [ ] Mails
  - Email preview
  - Email design
  - SMTP server config
- [ ] Jobs
- [ ] Storage
- [ ] Hotwire
  - Turbo Drive
  - Turbo Frames
  - Turbo Streams
  - Stimulus JS
- [ ] Action cables & Websockets
- [ ] Translations (i18n)
- [ ] Deployment
