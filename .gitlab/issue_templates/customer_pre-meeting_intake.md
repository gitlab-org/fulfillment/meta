## Intake

_Please fill out this form as a prerequisite to your customer meeting with a GitLab Fulfillment PM_

GitLab Team Member Details
- **Name & Role of Interal GitLab Person Requesting Meeting**:
- [ ] Check this box that you've set expectations with your customer. This call won't be for deep dive trouble shooting but rather be a way to understand the customer's workflow in order to influence our product roadmap. We want to make sure what we build helps solve customer pains, so being able to talk to customers about how they work & the issues they run into helps us do that!

Meeting Details
- **Purpose of Meeting**:
- **Ideal outcome of the Meeting**:
- **Additional context to share**:
- **Meeting notes doc**: 

Customer Details
- **Customer Name**:
- **SFDC Account Link**:
- **Customer's number of users**:
- **Customer since when if known**: 
- **If propspect, are they coming from a competitor? If so, which one?**
- **SaaS or SM** _(if SM, what version)_:
- **Are there any current or future features they are interested in learning about? Please provide any links to relevant issues:**
- **Any known complaints or challenges with GitLab as it relates to self-service subscription management or purchasing**:


/confidential
