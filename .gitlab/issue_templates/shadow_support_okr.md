
### Steps for shadowing exercise

- [ ] Coordinate with a support team member to setup a quick chat for shadowing. If sync meeting is not possible then tag the support team member in a comment in this issue and ask them to suggest a few slots in which the shadowing could be done. This [list](https://gitlab.com/gitlab-org/fulfillment-meta/-/issues/517#note_1001073233) could be used to find a support team member in your time zone.

- [ ] Consider combining shadow sessions with another team member. This makes more efficient usage of the shadowee's time.

- [ ] During the slot decided for shadowing, ask the support team member if they could loop you in on a couple of support tickets they are working on so that you can follow along. If the discussion is happening in a support team slack channel, join the slack channel temporarily while you are shadowing.

- [ ] Try to understand the problems that the support team members face when dealing with customer issues.

- [ ] During the shadowing process consider the below points. If you find useful insights, please add it to the [key takeaways](https://gitlab.com/gitlab-org/fulfillment-meta/-/issues/690#note_1082054803) thread.

	- [ ] Are there any missing data points with regards to Fulfillment data in any of the systems that support team uses which could be useful for them?

	- [ ] Are there any processes, queries etc that the support team runs frequently on Fulfillment data which can be baked into a separate tool for them?

- [ ] _[Optional]_ If there are any useful notes during the shadowing that could help, add a notes section in this issue’s description or a comment thread.

- [ ] Thank the support team member for giving their valuable time towards this exercise.

- [ ] Take a moment to go through the key takeaways and retrospective sections below and drop your thoughts.

- [ ] Thank you for spending your valuable time in shadowing a stakeholder. You can close this issue.

  
### Key Takeaways

If there are any useful insights that you found during the shadowing exercise, please consider sharing them in this [thread](https://gitlab.com/gitlab-org/fulfillment-meta/-/issues/690#note_1082054803). This could be process improvement, suggestions for new tools to be created for support team, enhancing logs or anything else that might benefit the support team.


### Retrospective

Did the shadowing exercise help in building empathy towards the support team?

- [ ] Yes

- [ ] No

-  _[Optional]_ Do you have suggestions for improving the shadowing process in the future? If yes, please consider posting them in the retrospective thread [here](https://gitlab.com/gitlab-org/fulfillment-meta/-/issues/690#note_1082059960) so that these suggestions can be incorporated for future shadowing exercises.

  
  
/label ~"devops::fulfillment" ~"section::fulfillment"