<!-- 

    The first four sections are strongly recommended in your first draft, 
    while the rest of the sections can be filled out during the problem validation or breakdown phase.
    However, keep in mind that providing complete and relevant information early 
    helps our product team validate the problem and start working on a solution.
    
    1. Problem to solve
    2. Intended users
    3. User experience goal
    4. Proposal
-->

[[_TOC_]]

## Problem to Solve

<!-- 
    What is the problem and solution you're proposing? 
-->


## Intended Users

<!-- 
    Who will use this feature? 
    User Personas relevant are described at: 
    https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#user-personas
-->

## User Experience Goal

<!-- 
    What is the single user experience workflow this problem addresses?
    For example, "The user should be able to use the UI/API/.gitlab-ci.yml with GitLab to <perform a specific task>"
    https://about.gitlab.com/handbook/engineering/ux/ux-research-training/user-story-mapping/ 
-->

## Proposal

<!-- 
    How are we going to solve the problem? 
    Try to include the user journey! https://about.gitlab.com/handbook/journeys/#user-journey 
-->

## Further Details

<!-- 
    Include details that will help us understand the problem better. 
    1. Use Cases
    2. Benefits
    3. Goals
-->

## What does success look like, and how can we measure that?

<!--
    Define both the success metrics.  Success metrics indicate the desired business outcomes.
    Define acceptance criteria.  Acceptance criteria indicate when the solution is working correctly. 
    If there is no way to measure success, link to an issue that will implement a way to measure this.

    Create tracking issue using the the Snowplow event tracking template. 
    See https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/issue_templates/Snowplow%20event%20tracking.md
-->

## What is the type of buyer?

<!-- 
    What is the buyer persona for this feature? 
    See https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/buyer-persona/
    In which enterprise tier should this feature go? See https://about.gitlab.com/handbook/product/pricing/#three-tiers 
-->

## Is this a cross-stage feature?

<!-- 
    Communicate if this change will affect multiple Stage Groups or product areas. 
    We recommend always start with the assumption that a feature request will have an impact into another Group. 
    Loop in the most relevant PM and Product Designer from that Group to provide strategic support 
    to help align the Group's broader plan and vision, as well as to avoid UX and technical debt.  
-->

| TOPIC | STAGE | DESCRIPTION | PRODUCT MANAGER | PRODUCT DESIGNER |
|:----  |:----- |:----------- |:------ |:-------- |
| | | | | |

## Is this a cross-team feature?

<!-- 
    Communicate if this change will affect multiple teams. 
    For fulfillment, we recommend to start with the assumption that a feature will have an impact into 
    Finance, Billing, Sales Ops, Legal, Ent-Ops. Loop in DRIs from each team to provide strategic support and 
    help align the Group's broader plan and vision  
-->

| TOPIC | TEAM | DESCRIPTION | DRIVER | APPROVER | CONTRIBUTORS | INFORMED |
|:----- |:---- |:----------- |:------ |:-------- |:------------ |:-------- |
| | | | | | | |

## Documentation

<!-- 
    See the Feature Change Documentation Workflow 
        https://docs.gitlab.com/ee/development/documentation/workflow.html#for-a-product-change
    * Add all known Documentation Requirements in this section. 
        See https://docs.gitlab.com/ee/development/documentation/workflow.html
    * If this feature requires changing permissions, update the permissions document. 
        See https://docs.gitlab.com/ee/user/permissions.html -->

