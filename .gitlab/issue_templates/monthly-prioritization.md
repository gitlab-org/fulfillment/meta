<!--
Title: [Group Name] 2022-XX MR prioritization review
-->

Please review the Cross-functional dashboards on the team's handbook page:
- [Fulfillment Platform](https://about.gitlab.com/handbook/engineering/development/fulfillment/fulfillment-platform/#mr-types-dashboard)
- [Provision](https://about.gitlab.com/handbook/engineering/development/fulfillment/provision/#merged-merge-request-types)
- [Utilization](https://about.gitlab.com/handbook/engineering/development/fulfillment/utilization/#merged-merge-request-types)
- [Purchase](TBU)
---
- [ ] Quad leadership ()
- [ ] Engineering Manager ()
- [ ] Product Manager ()
- [ ] Product Designer ()
- [ ] Software Engineer in Test (if applicable)

/assign [EM, PM, ...]

<!--
Copy the questions below as new threads in this issue:

### Are the undefined MR types over or under 5%? (If over 5%, what improvements can be made? - e.g. ensuring both issues and MRs are adequately tracked with a type label, or leveraging the /copy_metdata command on merged MRs.)

### Did we meet our predefined ratio goals (e.g. 60% features / 30% maintenance / 10% bugs)?

### Is our current ratio % working well for our team and is the quad aligned on these %s? (If the quad wishes to adjust these, create a handbook MR on your team page to discuss and ensure they're visible to the rest of the team.)

### Was it accurate in the past month? Why or why not? (If not, what improvements can be made?)
-->
